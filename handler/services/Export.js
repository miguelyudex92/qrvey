const { listProjects } = require('../dao/listProjects');
const { UploadS3 } = require('../utils/UploadS3');
const { GenerateExcel } = require('./GenerateExcel');

const ExportService = () => {
    return new Promise(async(resolve, reject) => {

        try {
            console.log('Consultando registros...');
            let rows = await listProjects();
            console.log('Se envían los datos para generar Excel...');
            let pathFile = await GenerateExcel(rows);
            let endpoint = await UploadS3(pathFile, "application/vnd.ms-excel");
            resolve(endpoint);
        } catch (error) {
            reject(error);
        }
    })
}

module.exports = {
    
    ExportService
}
