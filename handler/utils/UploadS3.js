const fs = require('fs');
const AWS = require('aws-sdk');
const s3 = new AWS.S3();

const UploadS3 = async (configs, contentType) => {
    return new Promise(async (resolve, reject) => {
        try {
            let url = "https://qrvey-files.s3.amazonaws.com/"
            let bucket = "qrvey-files";
            let file = fs.readFileSync(configs.path);
            let key = configs.nameFile;
            let endpoint = `${url}${key}`
            params = { Bucket: bucket, Key: key, Body: file, ACL: 'public-read', ContentType: contentType };

            await s3.putObject(params, (err, result) => {
                if (err) {
                    reject(err);
                    return;
                }
                resolve(`Archivo generado, puede descargar el archivo siguiendo el siguiente enlace: ${endpoint}`);
            })

        } catch (error) {
            reject(error);
        }
    })
}

module.exports = {
    UploadS3
}
