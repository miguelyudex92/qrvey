const { listProjects } = require('../dao/listProjects');
const { GeneratePDF } = require('./GeneratePDF');
const { UploadS3 } = require('../utils/UploadS3');

const CreatePDF = () => {
    return new Promise(async(resolve, reject) => {

        try {
            console.log('Consultando registros...');
            let rows = await listProjects();
            console.log('Se envían los datos para generar PDF...');
            let configs = await GeneratePDF(rows);
            let endpoint = await UploadS3(configs, "application/pdf");
            resolve(endpoint);
        } catch (error) {
            reject(error);
        }
    })
}

module.exports = {
    CreatePDF
}
