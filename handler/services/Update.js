const { updateProject } = require('../dao/updateProjectDao');

const UpdateService = async (payload) => {
    return new Promise(async (resolve, reject) => {
        try {
            await updateProject(payload)
            resolve()
        } catch (error) {
            reject(error)
        }

    })


}

module.exports = {
    UpdateService
}
