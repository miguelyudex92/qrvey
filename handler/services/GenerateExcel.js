const excelJS = require("exceljs");
const moment = require('moment');


const GenerateExcel = async (data) => {
    return new Promise(async (resolve, reject) => {
        try {
            const workbook = new excelJS.Workbook();
            const worksheet = workbook.addWorksheet("Projects");
            const path = "../../tmp/";
            const date = moment().format("DD-MM-YYYY_HHmmss");
            const nameFile = `project-${date}.xlsx`

            let headers = Object.keys(data[0]);

            let headersConfig = headers.map((value) => {
                return {
                    header: value,
                    key: value,
                    width: 20
                }
            });
            console.log(headersConfig);

            worksheet.columns = headersConfig;

            console.log(`== Rellenando las filas en archivo de Excel == `);
            data.forEach((project) => {
                worksheet.addRow(project);
            });

            console.log(`== Configurando la primera fila en negrita == `);
            
            worksheet.getRow(1).eachCell((cell) => {
                cell.font = { bold: true };
            });

            try {
                console.log(`== Creando archivo de Excel == `);
                await workbook.xlsx.writeFile(`${path}${nameFile}`);
                console.log(`== Archivo generado == `);
                resolve({path:`${path}${nameFile}`, nameFile});
            } catch (error) {
                reject(error);
            }
            
        } catch (error) {
            reject(error);
        }
    })
}

module.exports = {

    GenerateExcel
}
