const AWS = require('aws-sdk');
const moment = require('moment');
const dotenv = require('dotenv');
const uuid = require('uuid')

AWS.config.update({
    region: "us-east-1"
});

dotenv.config();

const docClient = new AWS.DynamoDB.DocumentClient({ region: 'us-east-1' });

//Registrar item en la tabla projects
const registerProject = (payload) => {
    return new Promise(async (resolve, reject) => {
        let table = process.env.TABLE_PROJECT

        let params = {
            TableName: table,
            Item: {
                
                "id_project": uuid.v4(),
                "name_project": payload?.name,
                "area": payload?.area,
                "budget": payload?.budget || 0,
                "creation_date": moment().format("DD/MM/YYYY HH:mm:ss")
            }
        }

        console.log(`Adding a new item in ${table}...`);

        docClient.put(params, (err, data) => {
            if (err) {
                console.error('No se pudo guardar el proyecto, Error: ', JSON.stringify(err));
                reject();
            } else {
                console.log("Proyecto agregado con éxito:", JSON.stringify(data));
                resolve();
            }
        })
    })

}

module.exports = {
    registerProject
}
