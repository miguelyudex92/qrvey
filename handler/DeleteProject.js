

const { DeleteService } = require('./services/Delete')
let response;

exports.lambdaHandler = async (event, context) => {
    try {
        
        let idProject;
        if (event.queryStringParameters && event.queryStringParameters.id) {
            console.log("Received id: " + event.queryStringParameters?.id);
            idProject = event.queryStringParameters.id;

            let data = await DeleteService(idProject);

            response = {
                statusCode: 200,
                headers: {
                    "Access-Control-Allow-Headers": "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
                    "Access-Control-Allow-Origin": "*",
                    "Access-Control-Allow-Methods": "OPTIONS, POST"
                },
                body: JSON.stringify({ message: data })
            }
        }else{
            response = {
                statusCode: 400,
                headers: {
                    "Access-Control-Allow-Headers": "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
                    "Access-Control-Allow-Origin": "*",
                    "Access-Control-Allow-Methods": "OPTIONS, POST"
                },
                body: JSON.stringify({ message: "Debe enviar id del proyecto." })
            } 
        }



    } catch (err) {
        console.log('Ha ocurrido un error: ', err)
        response = {
            statusCode: 500,
            headers: {
                "Access-Control-Allow-Headers": "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
                "Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Methods": "OPTIONS, POST"
            },
            body: JSON.stringify({
                message: 'Ocurrió un error en la función.',
                errorMessage: JSON.stringify(err)
            })
        }
    }

    return response
};

//Local Test 
//sam local invoke -e ./events/eventDeleteProject.json DeleteProject