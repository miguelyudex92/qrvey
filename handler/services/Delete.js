const { deleteProjectDao } = require('../dao/deleteProjectDao');

const DeleteService = (id) => {

    return new Promise(async(resolve, reject) => {
        try {
            let data = await deleteProjectDao(id);
            resolve(data);
        } catch (error) {
            reject(error);
        }
    })
}

module.exports = {
    DeleteService
}
