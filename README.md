# README #

Prueba técnica Qrvey, API de reporte de proyectos, se puede crear, listar, actualizar y borrar proyectos, también se puede generar archivos en excel de todos los proyectos creados, generar pdf de reporte de los proyectos.

### Stack utilizado ###

* Node.js
* AWS Lambda
* AWS Dynamo DB
* AWS S3
* SAM 

### Servicios Construidos ###

* CreateProject
* ListProjects
* UpdateProject
* DeleteProject
* GeneratePDF
* GenerateExcel

### Endpoints ###

* https://k941sxh37b.execute-api.us-east-1.amazonaws.com/Prod/create-project | Método http: POST
* https://k941sxh37b.execute-api.us-east-1.amazonaws.com/Prod/list-projects | Método http: GET
* https://k941sxh37b.execute-api.us-east-1.amazonaws.com/Prod/update-project | Método http: PUT
* https://k941sxh37b.execute-api.us-east-1.amazonaws.com/Prod/delete-project | Método http: DELETE | queryString : id
* https://k941sxh37b.execute-api.us-east-1.amazonaws.com/Prod/pdf | Método http: GET
* https://k941sxh37b.execute-api.us-east-1.amazonaws.com/Prod/export | Método http: GET

### Pruebas unitarias
1. Create Project
   - Endpoint: https://k941sxh37b.execute-api.us-east-1.amazonaws.com/Prod/create-project
   - Method: POST
   - Body: {
        "name": "Project QA",
        "area": "QA 1",
        "budget": 100
    }

![Create Project](images/test-createProject.JPG)

2. List Projects
   - Endpoint: https://k941sxh37b.execute-api.us-east-1.amazonaws.com/Prod/list-projects |
   - Method: GET
   
![List Projects](images/test-listProjects.JPG)

3. Update Project
   - Endpoint: https://k941sxh37b.execute-api.us-east-1.amazonaws.com/Prod/update-project |
   - Method: PUT
   
![Update Project](images/test-updateProject.JPG)

4. Delete Project
   - Endpoint: https://k941sxh37b.execute-api.us-east-1.amazonaws.com/Prod/delete-project | querystring : id
   - Method: DELETE
   
![Delete Project](images/test-deleteProject.JPG)

5. Export PDF
   - Endpoint: https://k941sxh37b.execute-api.us-east-1.amazonaws.com/Prod/pdf
   - Method: GET
   
![Export PDF](images/test-export-pdf.JPG)

* PDF Generado, se debe copiar la url devuelta por la petición:

![PDF Generado](images/pdf-generado.JPG)

6. Export Excel
   - Endpoint: https://k941sxh37b.execute-api.us-east-1.amazonaws.com/Prod/export
   - Method: GET
   
![Export PDF](images/test-export-excel.JPG)

* Excel Generado, se debe copiar la url devuelta por la petición:

![Excel Generado](images/excel-generado.JPG)

