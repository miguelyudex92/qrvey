const _ = require('lodash');
const { UpdateService } = require('./services/Update');

let response;

exports.lambdaHandler = async (event, context) => {
    
    let dataReceived = JSON.parse(JSON.stringify(event.body));
    let body = JSON.parse(dataReceived);

    if (body) {
        try {
            await UpdateService(body)

            response = {
                statusCode: 200,
                headers: {
                    "Access-Control-Allow-Headers": "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
                    "Access-Control-Allow-Origin": "*",
                    "Access-Control-Allow-Methods": "OPTIONS, POST"
                },
                body: JSON.stringify({ message: 'Proyecto Actualizado correctamente' })
            }

        } catch (err) {
            console.log('Ha ocurrido un error: ', err)
            response = {
                statusCode: 500,
                headers: {
                    "Access-Control-Allow-Headers": "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
                    "Access-Control-Allow-Origin": "*",
                    "Access-Control-Allow-Methods": "OPTIONS, POST"
                },
                body: JSON.stringify({
                    message: 'Ocurrió un error en la función.',
                    errorMessage: JSON.stringify(err)

                })
            }
        }
    } else {
        response = {
            statusCode: 400,
            headers: {
                "Access-Control-Allow-Headers": "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
                "Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Methods": "OPTIONS, POST"
            },
            body: JSON.stringify({
                message: 'No se encontraron datos para actualizar'
            })
        }
    }



    return response
};

//sam local invoke -e ./events/eventUpdateProject.json UpdateProject