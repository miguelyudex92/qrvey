const AWS = require('aws-sdk');
const dotenv = require('dotenv');

AWS.config.update({
    region: "us-east-1"
});

dotenv.config();

const docClient = new AWS.DynamoDB.DocumentClient({ region: 'us-east-1' });

//Actualizar item en la tabla projects
const updateProject = (payload) => {
    return new Promise(async (resolve, reject) => {
        let table = process.env.TABLE_PROJECT

        let params = {
            TableName: table,
            Key: {
                "id_project": payload?.id
            },
            UpdateExpression: "set area =  :area, name_project = :nameProject, budget = :budget",
            ExpressionAttributeValues: {
                ":area": payload.area ?? "",
                ":nameProject": payload.name ?? "",
                ":budget":payload.budget ?? 0
            },
            ReturnValues: "UPDATED_NEW"
        }

        console.log(`Update item in ${table}...`);

        docClient.update(params, (err, data) => {
            if (err) {
                console.error('No se pudo actualizar el registro, Error: ', JSON.stringify(err))
                reject(err);
            } else {
                console.log("Registro actualizado con éxito:", JSON.stringify(data));
                resolve(data);
            }
        })
    })

}

module.exports = {
    updateProject
}
