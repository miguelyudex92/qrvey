const PDFDocument = require("pdfkit-table");
const moment = require('moment');
const fs = require('fs');

const GeneratePDF = async (data) => {

    return new Promise(async (resolve, reject) => {
        try {
            // create document
            let doc = new PDFDocument({ margin: 30, size: 'A4' });
            const path = "../../tmp/";
            const date = moment().format("DD-MM-YYYY_HHmmss");
            const nameFile = `project-${date}.pdf`;

            // file name
            let stream = fs.createWriteStream(`${path}${nameFile}`);
            doc.pipe(stream);

            let headersData = Object.keys(data[0]);
            let rowsData = data.map((value) => {
                return Object.values(value);
            });
            

            const tableArray = {
                title: "Projects Report",
                subtitle: "Qrvey",
                headers: headersData,
                rows: rowsData,
            };
            await doc.table(tableArray, {width: 500});

            doc.end();

            stream.on('close', () => {
                resolve({ path: `${path}${nameFile}`, nameFile });
            });

        } catch (error) {
            reject(error);
        }
    })
}

module.exports = {
    GeneratePDF
}
