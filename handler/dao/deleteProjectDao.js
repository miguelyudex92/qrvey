const AWS = require('aws-sdk');
const moment = require('moment');
const dotenv = require('dotenv');

AWS.config.update({
    region: "us-east-1"
});

dotenv.config();

const docClient = new AWS.DynamoDB.DocumentClient({ region: 'us-east-1' });

//Elimina item en la tabla projects
const deleteProjectDao = (_id) => {
    return new Promise(async (resolve, reject) => {
        try {
            let table = process.env.TABLE_PROJECT

            let params = {
                TableName: table,
                Key: {
                    id_project: _id
                }
            }
            console.log(`Deleting a item with id ${_id} in ${table}...`);

            await docClient.delete(params, (err, data) => {
                if(err) reject(err);
                resolve(`Registro con id ${_id} eliminado correctamente.`);
            })
            
        } catch (error) {
            reject(error);
        }
    })

}

module.exports = {
    deleteProjectDao
}
