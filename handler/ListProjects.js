

const { ListService } = require('./services/List')
let response;

exports.lambdaHandler = async (event, context) => {
    try {
        console.log(event);
        let data = await ListService();

        response = {
            statusCode: 200,
            headers: {
                "Access-Control-Allow-Headers": "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
                "Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Methods": "OPTIONS, POST"
            },
            body: JSON.stringify({ message: data })
        }


    } catch (err) {
        console.log('Ha ocurrido un error: ', err)
        response = {
            statusCode: 500,
            headers: {
                "Access-Control-Allow-Headers": "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
                "Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Methods": "OPTIONS, POST"
            },
            body: JSON.stringify({
                message: 'Ocurrió un error en la función.',
                errorMessage: JSON.stringify(err)
            })
        }
    }

    return response
};

//Test Local
//sam local invoke -e ./events/eventListProject.json ListProject
