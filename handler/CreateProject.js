const _ = require('lodash');
const { CreateService } = require('./services/Create');

let response;

exports.lambdaHandler = async (event, context) => {
    
    let dataReceived = JSON.parse(JSON.stringify(event.body));
    let body = JSON.parse(dataReceived);

    if (body.hasOwnProperty('name') && body.hasOwnProperty('area')) {
        try {
            await CreateService(body)

            response = {
                statusCode: 200,
                headers: {
                    "Access-Control-Allow-Headers": "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
                    "Access-Control-Allow-Origin": "*",
                    "Access-Control-Allow-Methods": "OPTIONS, POST"
                },
                body: JSON.stringify({ message: 'Proyecto guardado correctamente' })
            }

        } catch (err) {
            console.log('Ha ocurrido un error: ', err)
            response = {
                statusCode: 500,
                headers: {
                    "Access-Control-Allow-Headers": "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
                    "Access-Control-Allow-Origin": "*",
                    "Access-Control-Allow-Methods": "OPTIONS, POST"
                },
                body: JSON.stringify({
                    message: 'Ocurrió un error en la función.',
                    errorMessage: JSON.stringify(err)

                })
            }
        }
    } else {
        response = {
            statusCode: 400,
            headers: {
                "Access-Control-Allow-Headers": "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
                "Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Methods": "OPTIONS, POST"
            },
            body: JSON.stringify({
                message: 'Los parametros name y area son obligatorios.'
            })
        }
    }



    return response
};

//Local Test
//sam local invoke -e ./events/eventCreateProject.json CreateProject