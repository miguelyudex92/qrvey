const { listProjects } = require('../dao/listProjects');

const ListService = () => {
    return new Promise(async(resolve, reject) => {
        try {
            let data = await listProjects();
            resolve(data);
        } catch (error) {
            reject(error);
        }
    })
}

module.exports = {
    ListService
}
