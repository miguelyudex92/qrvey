const AWS = require('aws-sdk');
const moment = require('moment');
const dotenv = require('dotenv');
const uuid = require('uuid')

AWS.config.update({
    region: "us-east-1"
});

dotenv.config();

const docClient = new AWS.DynamoDB.DocumentClient({ region: 'us-east-1' });

//Registrar item en la tabla projects
const listProjects = () => {
    return new Promise(async (resolve, reject) => {

        let table = process.env.TABLE_PROJECT

        let params = {
            TableName: table,
            Limit: 100
        }

        console.log(`Searching items in ${table}...`);

        docClient.scan(params, (err, data) => {
            if (err) {
                console.error(
                    "Unable to query. Error:",
                    JSON.stringify(err, null, 2)
                );
                return reject(err);
            } else {
                console.log("Query succeeded.");
                return resolve(data.Items);
            }
        })
    })

}

module.exports = {
    listProjects
}
