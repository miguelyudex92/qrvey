const { CreatePDF } = require('./services/CreatePDF');
let response;

exports.lambdaHandler = async (event, context) => {
    try {

        let endpoint = await CreatePDF();

        response = {
            statusCode: 200,
            headers: {
                "Access-Control-Allow-Headers": "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
                "Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Methods": "OPTIONS, POST"
            },
            body: JSON.stringify({ message: endpoint })
        }


    } catch (err) {
        console.log('Ha ocurrido un error: ', err);
        response = {
            statusCode: 500,
            headers: {
                "Access-Control-Allow-Headers": "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
                "Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Methods": "OPTIONS, POST"
            },
            body: JSON.stringify({
                message: 'Ocurrió un error en la función.',
                errorMessage: JSON.stringify(err)
            })
        }
    }

    return response
};

//Test Local
//sam local invoke -e ./events/eventCreatePDF.json PDF