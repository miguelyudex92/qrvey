const { registerProject } = require('../dao/registerProject');

const CreateService = async (payload) => {
    return new Promise(async (resolve, reject) => {
        try {
            await registerProject(payload)
            resolve()
        } catch (error) {
            reject(error)
        }

    })


}

module.exports = {
    CreateService
}
